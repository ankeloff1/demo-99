package com.example.demo98;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Demo98Application {

    public static void main(String[] args) {
        SpringApplication.run(Demo98Application.class, args);
    }

}
